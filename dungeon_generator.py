import numpy as np
import random
from PIL import Image

BLANK = '0'

"""
use classes?

exit = {x: _, y: _, type: "", direction: ""}

door = {x: _, y: _, type: "", direction: ""}

room = {id: _, contents: ""}

[[0,0,0,0,1,1,1,1,0,0,0,0],
                    [0,0,0,1,1,1,1,1,1,0,0,0],
                    [0,0,1,1,1,1,1,1,1,1,0,0],
                    [0,1,1,1,1,1,1,1,1,1,1,0],
                    [1,1,1,1,1,1,1,1,1,1,1,1],
                    [1,1,1,1,1,1,1,1,1,1,1,1],
                    [1,1,1,1,1,1,1,1,1,1,1,1],
                    [1,1,1,1,1,1,1,1,1,1,1,1],
                    [0,1,1,1,1,1,1,1,1,1,1,0],
                    [0,0,1,1,1,1,1,1,1,1,0,0],
                    [0,0,0,1,1,1,1,1,1,0,0,0],
                    [0,0,0,0,1,1,1,1,0,0,0,0]]
"""

class Exit:
    """class for storing info about an exit"""
    def __init__(self, x, y, t, d):
        self.x = x
        self.y = y
        self.exit_type = t
        self.direction = d

class DungeonMap:
    """class for storing info for and generating a dungeon map"""
    def __init__(self, size_x, size_y):
        """initialize a blank map"""
        self.max_x = size_x - 1
        self.max_y = size_y - 1
        self._map = [[BLANK] * size_x] * size_y
        self._map = np.array(self._map)
        self.list_of_exits = []
        self.list_of_rooms = []
        self.attempt_max = 6
    
    def generate(self, x_start=50, y_start=50, num_rooms=10):
        """main function to generate a dungeon map"""
        # if blank map
        #   create room
        # for each exit
        #   create room/hall
        #   update exit list with new exits
        # if exits exist go back to 3
        random.seed()
        if len(self.list_of_exits) == 0:
            print("Creating Starting room at (50, 50).")
            temp_exit = Exit(x_start, y_start, "start", 'r')
            self.create_new_room(temp_exit)
        while len(self.list_of_exits) > 0 and len(self.list_of_rooms) < num_rooms:
            print(f"Remaining exits: {len(self.list_of_exits)}") 
            # breadth first expansion
            # TODO : expansion based on nearby whitespace?
            # TODO : expansion based on a different heuristic?
            e = self.list_of_exits.pop(0)
            if e.exit_type == "door":
                print(f"Expanding dungeon via the door at ({e.x}, {e.y}).")
                self.beyond_a_door(e)
            else:
                print(f"Expanding dungeon via the hall at ({e.x}, {e.y}).")
                self.continue_hall(e)

    def output_as_text(self, output_file="output.txt"):
        """display the map"""
        with open(output_file, 'w') as file:
            for i in range(self.max_x):
                for j in range(self.max_y):
                    try:
                        if self._map[j][i] == BLANK:
                            file.write("-")
                        else:
                            file.write(self._map[j][i])
                    except IndexError:
                        print("Hit outside array index.")
                file.write("\n")

    def output_as_img(self, output_file="output.png", square_size=8):
        """"""
        # dim = (self.max_y + 1, self.max_x + 1, 3)
        # data = np.zeros(dim, dtype=np.uint8)
        dim = ((self.max_y+1)*square_size, (self.max_x+1)*square_size, 3)
        big_data = np.zeros(dim, dtype=np.uint8)
        for i in range(self.max_x + 1):
            for j in range(self.max_y + 1):
                # get character rgb
                character = self._map[j][i]
                rgb = [0, 0, 0]
                if character != BLANK:
                    if character == 'D':
                        rgb = [160, 82, 45] # sienna (brown)
                    elif character == 'H':
                        rgb = [0, 0, 255] # blue
                    else:
                        rgb = [192, 192, 192] # silver
                # fill square        
                j_shift = j * square_size
                i_shift = i * square_size
                for x in range(square_size):
                    for y in range(square_size):
                        big_data[x + j_shift][y + i_shift] = rgb
        img = Image.fromarray(big_data, "RGB")
        # img = img.resize(((self.max_x+1) * square_size, (self.max_y+1) * square_size))
        img.save(output_file)
        img.show()

    def insert(self, value, x, y):
        if x > self.max_x or y > self.max_y:
            return False
        if x < 0 or y < 0:
            return False
        if self._map[x][y] == BLANK:
            self._map[x][y] = str(value)
            return True
        return False
        
    def replace(self, value, x, y):
        if x > self.max_x or y > self.max_y:
            return False
        if x < 0 or y < 0:
            return False
        self._map[x][y] = str(value)
        return True

    def beyond_a_door(self, _exit):
        """determine what's behind a door"""
        # TODO : if fail to generate one, try another
        value = random.randint(1,18) # TODO : increase to 20
        if value in [1,2,3,4,5,6,7,8]:
            self.create_new_hall(_exit)
        elif value in [9,10,11,12,13,14,15,16,17,18]:
            self.create_new_room(_exit)
        elif value in [19]:
            # stairs
            pass
        else:
            # trap
            pass

    def continue_hall(self, _exit):
        """determine how a hall continues"""
        value = random.randint(1,19) # TODO : increase to 20
        if value in [1,2,3,4,5,6,7,8,9,10,11,12,13,14]:
            self.create_new_hall(_exit)
        elif value in [15,16,17,18,19]:
            self.create_new_room(_exit)
        else:
            # stairs
            pass

    def create_new_room(self, _exit):
        """create and place a new room connected to the exit at (ex, ey)"""
        # create room shape/size
        # align room against (ex, ey)
        # repeat creation if doesn't fit (how to determine?)
        # place room in map
        # generate exits and add to list of exits
        # generate contents and add to list of rooms
        room_id = len(self.list_of_rooms) + 1
        print(f"Attmepting to create a room with ID {room_id} at ({_exit.x}, {_exit.y}).")
        attempts = 0
        valid = False
        start_x = -1
        start_y = -1
        while not valid and attempts < self.attempt_max:
            room = self._get_random_room()
            print(room)
            start_x, start_y, align_value = self.align_new_room(_exit, room)
            if start_x == -1 or start_y == -1:
                pass
            else:
                valid = True
            attempts += 1

        if not valid:
            print("Could not create a room.")
            return

        for i in range(len(room[0])):
            for j in range(len(room)):
                # i = column
                # j = row
                if str(room[j][i]) != BLANK:
                    # insert or replace? does room or hall have precedence?
                    self.replace(chr(room_id + 96), start_x + i, start_y + j)
        # TODO : better way of connecting 
        self.connect_exit_to_room(_exit, room_id)

        x_range = (start_x, start_x + len(room[0]) - 1)
        y_range = (start_y, start_y + len(room) - 1)
        num_exits = random.randint(0,2)
        if _exit.exit_type == "start":
            num_exits += 1
        for i in range(num_exits):
            new_exit = self.create_new_exit(x_range, y_range)
            self.list_of_exits.append(new_exit)
            self.connect_room_to_exit(new_exit, room_id)

        contents = "something will eventuall go here" # TODO
        self.list_of_rooms.append(contents)

    def create_new_hall(self, _exit):
        """create and place a new hall connected to the exit at (ex, ey)"""
        # create hall
        # repeat creation if it doesn't fit (how to determine?)
        # place hall in map
        # generate exits and add to list of exits
        print(f"Attempting to create a hallway at ({_exit.x}, {_exit.y}).")
        attempts = 0
        valid = False
        ex = _exit.x
        ey = _exit.y
        start_x = -1
        start_y = -1
        while not valid and attempts < self.attempt_max:
            # reset exit coordinates so shift doesn't break the generation
            _exit.x = ex
            _exit.y = ey
            hall, exits, shift = self._get_random_hall(_exit)
            if _exit.direction in ['u', 'd']:
                _exit.x = _exit.x + shift
            else:
                _exit.y = _exit.y + shift
            start_x, start_y, align_value = self.align_new_hall(_exit, hall, exits)
            if start_x == -1 or start_y == -1:
                pass
            else:
                valid = True
            attempts += 1

        if not valid:
            print("Could not continue hallway.")
            return

        # the shift value is typically zero, but some of the turning halls need slight adjustment
        # if _exit.direction in ['u', 'd']:
        #     start_x += shift
        # else:
        #     start_y += shift

        for i in range(len(hall[0])):
            for j in range(len(hall)):
                if str(hall[j][i]) != BLANK:
                    self.insert('H', start_x + i, start_y + j)

        for e in exits:
            if e.exit_type == "door":
                self.replace('D', e.x, e.y)
            self.list_of_exits.append(e)

    def create_new_exit(self, x_range, y_range):
        """"""
        # choose type (door or hall)
        # choose side
        # determine direction
        # determine coordinates
        # TODO : validate exit location 
        value = random.randint(1,2)
        exit_type = "door" if value == 1 else "hall"
        value = random.randint(1,4)
        if value == 1:
            exit_direction = 'u'
            x_value = random.randint(x_range[0], x_range[1])
            y_value = y_range[0] - 1
        elif value == 2:
            exit_direction = 'd'
            x_value = random.randint(x_range[0], x_range[1])
            y_value = y_range[1] + 1
        elif value == 3:
            exit_direction = 'l'
            x_value = x_range[0] - 1
            y_value = random.randint(y_range[0], y_range[1])
        else:
            exit_direction = 'r'
            x_value = x_range[1] + 1
            y_value = random.randint(y_range[0], y_range[1])
        exit_obj = Exit(x_value, y_value, exit_type, exit_direction)
        if exit_type == "door":
            self.replace('D', x_value, y_value)
        else:
            self.replace('H', x_value, y_value)
        return exit_obj

    def align_new_room(self, _exit, new_room):
        """get alignment for new room in the map based on the given exit"""
        ex, ey = _exit.x, _exit.y
        dx = len(new_room[0]) - 1
        dy = len(new_room) - 1
        align_max = (dy if _exit.direction in ['l', 'r'] else dx) - 1
        align_value = -1
        start_x = -1
        stop_x = -1
        start_y = -1
        stop_y = -1
        for i in range(align_max):
            align_value = -1
            if _exit.direction == 'u':
                start_x = ex - i
                stop_x = ex + dx - i
                start_y = ey - dy #- 1
                stop_y = ey #- 1
            elif _exit.direction == 'd':
                start_x = ex - i
                stop_x = ex + dx - i
                start_y = ey #+ 1
                stop_y = ey + dy #+ 1
            elif _exit.direction == 'l':
                start_x = ex - dx #- 1
                stop_x = ex #- 1
                start_y = ey - i
                stop_y = ey + dy - i
            else:
                start_x = ex #+ 1
                stop_x = ex + dy #+ 1
                start_y = ey - i
                stop_y = ey + dy - i
            consideration = self._map[start_y : stop_y , start_x : stop_x]
            if any([c != BLANK for c in consideration.flatten()]):
                continue
            if stop_x > self.max_x or stop_y > self.max_y:
                continue
            align_value = i
            break
        if align_value == -1:
            return -1, -1, -1
        else:
            return start_x, start_y, align_value 

    def align_new_hall(self, _exit, new_hall, hall_exits):
        """align the new hall according to the given _exit and adjust the hall's exits"""
        ex, ey = _exit.x, _exit.y
        dx = len(new_hall[0]) - 1
        dy = len(new_hall) - 1
        start_x = -1
        start_y = -1
        valid = True
        try:
            if _exit.direction == 'u':
                start_x = ex
                stop_x = ex + dx
                start_y = ey - dy #- 1
                stop_y = ey #- 1
            elif _exit.direction == 'd':
                start_x = ex
                stop_x = ex + dx
                start_y = ey #+ 1
                stop_y = ey + dy #+ 1
            elif _exit.direction == 'l':
                start_x = ex - dx #- 1
                stop_x = ex #- 1
                start_y = ey
                stop_y = ey + dy
            else:
                start_x = ex #+ 1
                stop_x = ex + dy #+ 1
                start_y = ey
                stop_y = ey + dy
            if stop_x > self.max_x or stop_y > self.max_y:
                valid = False
            consideration = self._map[start_y : stop_y , start_x : stop_x]
            # print(consideration)
            # if any([c != BLANK for c in consideration.flatten()]):
            for i in range(dx):
                for j in range(dy):
                    # i = column
                    # j = row
                    # if the hall being inserted has a blank in this spot:
                    if new_hall[j][i] == BLANK:
                        continue
                    # if the hall being inserted doesn't have a blank in this spot AND
                    # if the consideration section has a blank in this spot:
                    if consideration[j][i] == BLANK:
                        continue
                    # if the new hall has a non-blank in the same place as the consideration section:
                    else:
                        valid = False
        except IndexError:
            valid = False
        if not valid:
            return -1, -1, -1
        return start_x, start_y, -1 

    def connect_exit_to_room(self, _exit, room_id):
        """connect exit to a room (room is spawned at the exit)"""
        ex = _exit.x
        ey = _exit.y
        character = chr(room_id + 96)
        spaces = 1
        while True:
            successful = None
            if _exit.exit_type == "start":
                break
            elif _exit.direction == 'u':
                successful = self.insert(character, ex, ey - spaces)
            elif _exit.direction == 'd':
                successful = self.insert(character, ex, ey + spaces)
            elif _exit.direction == 'l':
                successful = self.insert(character, ex - spaces, ey)
            else:
                successful = self.insert(character, ex + spaces, ey)
            # break when we can't insert - means there is a non-blank in that space
            if not successful:
                break
            spaces += 1

    def connect_room_to_exit(self, _exit, room_id):
        """connect a room to an exit (exit is spawned off the room)"""
        ex = _exit.x
        ey = _exit.y
        character = chr(room_id + 96)
        spaces = 1
        while True:
            successful = None
            if _exit.direction == 'u':
                successful = self.insert(character, ex, ey + spaces)
            elif _exit.direction == 'd':
                successful = self.insert(character, ex, ey - spaces)
            elif _exit.direction == 'l':
                successful = self.insert(character, ex + spaces, ey)
            else:
                successful = self.insert(character, ex - spaces, ey)
            # break when we can't insert - means there is a non-blank in that space
            if not successful:
                break
            spaces += 1

    def _get_random_room(self):
        """get a random room"""
        value = random.randint(1,20)
        if value in [1,2]:
            return [['1'] * 4] * 4
        elif value in [3,4]:
            return [['1'] * 6] * 6
        elif value in [5,6]:
            return [['1'] * 8] * 8
        elif value in [7,8,9]:
            return [['1'] * 4] * 6
        elif value in [10,11,12]:
            return [['1'] * 6] * 8
        elif value in [13,14]:
            return [['1'] * 8] * 10
        elif value == 15:
            return [['1'] * 10] * 16
        elif value == 16:
            return [[0,0,1,1,0,0],
                    [0,1,1,1,1,0],
                    [1,1,1,1,1,1],
                    [1,1,1,1,1,1],
                    [0,1,1,1,1,0],
                    [0,0,1,1,0,0]]
        elif value == 17:
            return [[0,0,0,0,1,1,0,0,0,0],
                    [0,0,1,1,1,1,1,1,0,0],
                    [0,1,1,1,1,1,1,1,1,0],
                    [0,1,1,1,1,1,1,1,1,0],
                    [1,1,1,1,1,1,1,1,1,1],
                    [1,1,1,1,1,1,1,1,1,1],
                    [0,1,1,1,1,1,1,1,1,0],
                    [0,1,1,1,1,1,1,1,1,0],
                    [0,0,1,1,1,1,1,1,0,0],
                    [0,0,0,0,1,1,0,0,0,0]]
        elif value == 18:
            return [[0,0,0,1,1,0,0,0],
                    [0,0,1,1,1,1,0,0],
                    [0,1,1,1,1,1,1,0],
                    [1,1,1,1,1,1,1,1],
                    [1,1,1,1,1,1,1,1],
                    [0,1,1,1,1,1,1,0],
                    [0,0,1,1,1,1,0,0],
                    [0,0,0,1,1,0,0,0]]
        elif value == 19:
            return [[0,0,0,1,1,0,0,0],
                    [0,0,1,1,1,1,0,0],
                    [0,1,1,1,1,1,1,0],
                    [1,1,1,1,1,1,1,1]]
        else:
            return [[0,0,0,1],
                    [0,0,1,1],
                    [0,1,1,1],
                    [1,1,1,1],
                    [1,1,1,1],
                    [0,1,1,1],
                    [0,0,1,1],
                    [0,0,0,1]]

    def _get_random_hall(self, _exit):
        """get a random hall"""
        d = _exit.direction
        ex = _exit.x
        ey = _exit.y
        shift = 0
        value = random.randint(1,14) # TODO : bump up to 15
        if value in [1,2]:
            if d == 'u':
                hall = [[1]] * 6
                exits = [Exit(ex, ey-6, "hall", d)]
            elif d == 'd':
                hall = [[1]] * 6
                exits = [Exit(ex, ey+6, "hall", d)]
            elif d == 'l':
                hall = [[1] * 6]
                exits = [Exit(ex-6, ey, "hall", d)]
            else:
                hall = [[1] * 6]
                exits = [Exit(ex+6, ey, "hall", d)]
        elif value == 3:
            if d == 'u':
                hall = [[1]] * 7
                exits = [Exit(ex, ey-7, "hall", d),
                         Exit(ex, ey-5, "door", 'r')]
            elif d == 'd':
                hall = [[1]] * 7
                exits = [Exit(ex, ey+7, "hall", d),
                         Exit(ex, ey+5, "door", 'l')]
            elif d == 'l':
                hall = [[1] * 7]
                exits = [Exit(ex-7, ey, "hall", d),
                         Exit(ex-5, ey, "door", 'u')]
            else:
                hall = [[1] * 7]
                exits = [Exit(ex+7, ey, "hall", d),
                         Exit(ex+5, ey, "door", 'd')]
        elif value == 4:
            if d == 'u':
                hall = [[1]] * 7
                exits = [Exit(ex, ey-7, "hall", d),
                         Exit(ex, ey-5, "door", 'l')]
            elif d == 'd':
                hall = [[1]] * 7
                exits = [Exit(ex, ey+7, "hall", d),
                         Exit(ex, ey+5, "door", 'r')]
            elif d == 'l':
                hall = [[1] * 7]
                exits = [Exit(ex-7, ey, "hall", d),
                         Exit(ex-5, ey, "door", 'd')]
            else:
                hall = [[1] * 7]
                exits = [Exit(ex+7, ey, "hall", d),
                         Exit(ex+5, ey, "door", 'u')]
        elif value == 5:
            if d == 'u':
                hall = [[1]] * 4
                exits = [Exit(ex, ey-4, "door", d)]
            elif d == 'd':
                hall = [[1]] * 4
                exits = [Exit(ex, ey+4, "door", d)]
            elif d == 'l':
                hall = [[1] * 4]
                exits = [Exit(ex-4, ey, "door", d)]
            else:
                hall = [[1] * 4]
                exits = [Exit(ex+4, ey, "door", d)]
        elif value in [6,7]:
            if d == 'u':
                hall = [[1]] * 7
                exits = [Exit(ex, ey-7, "hall", d),
                         Exit(ex, ey-5, "hall", 'r')]
            elif d == 'd':
                hall = [[1]] * 7
                exits = [Exit(ex, ey+7, "hall", d),
                         Exit(ex, ey+5, "hall", 'l')]
            elif d == 'l':
                hall = [[1] * 7]
                exits = [Exit(ex-7, ey, "hall", d),
                         Exit(ex-5, ey, "hall", 'u')]
            else:
                hall = [[1] * 7]
                exits = [Exit(ex+7, ey, "hall", d),
                         Exit(ex+5, ey, "hall", 'd')]
        elif value in [8,9]:
            if d == 'u':
                hall = [[1]] * 7
                exits = [Exit(ex, ey-7, "hall", d),
                         Exit(ex, ey-5, "hall", 'l')]
            elif d == 'd':
                hall = [[1]] * 7
                exits = [Exit(ex, ey+7, "hall", d),
                         Exit(ex, ey+5, "hall", 'r')]
            elif d == 'l':
                hall = [[1] * 7]
                exits = [Exit(ex-7, ey, "hall", d),
                         Exit(ex-5, ey, "hall", 'd')]
            else:
                hall = [[1] * 7]
                exits = [Exit(ex+7, ey, "hall", d),
                         Exit(ex+5, ey, "hall", 'u')]
        elif value == 10:
            # TODO : secret passage (10%)
            if d in ['u', 'd']:
                hall = [[1]] * 3
                exits = []
            else:
                hall = [[1] * 3]
                exits = []
        elif value in [11,12]:
            if d == 'u':
                hall = [[1,1,1],
                        [0,0,1],
                        [0,0,1],
                        [0,0,1]]
                exits = [Exit(ex-2, ey-4, "hall", 'l')]
                shift = -2
            elif d == 'd':
                hall = [[1,0,0],
                        [1,0,0],
                        [1,0,0],
                        [1,1,1]]
                exits = [Exit(ex+2, ey+4, "hall", 'r')]
            elif d == 'l':
                hall = [[1,1,1,1],
                        [1,0,0,0],
                        [1,0,0,0]]
                exits = [Exit(ex-4, ey+2, "hall", 'd')]
            else:
                hall = [[0,0,0,1],
                        [0,0,0,1],
                        [1,1,1,1]]
                exits = [Exit(ex+4, ey-2, "hall", 'u')]
                shift = -2
        elif value in [13,14]:
            if d == 'u':
                hall = [[1,1,1],
                        [1,0,0],
                        [1,0,0],
                        [1,0,0]]
                exits = [Exit(ex+2, ey-4, "hall", 'r')]
            elif d == 'd':
                hall = [[0,0,1],
                        [0,0,1],
                        [0,0,1],
                        [1,1,1]]
                exits = [Exit(ex-2, ey+4, "hall", 'l')]
                shift = -2
            elif d == 'l':
                hall = [[1,0,0,0],
                        [1,0,0,0],
                        [1,1,1,1]]
                exits = [Exit(ex-4, ey-2, "hall", 'u')]
                shift = -2
            else:
                hall = [[1,1,1,1],
                        [0,0,0,1],
                        [0,0,0,1]]
                exits = [Exit(ex+4, ey+2, "hall", 'd')]
        else:
            # TODO : stairs
            pass
        return hall, exits, shift
