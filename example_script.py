from dungeon_generator import DungeonMap

dungeon_map = DungeonMap(100, 100)
dungeon_map.generate(x_start=50, y_start=50, num_rooms=10)
dungeon_map.output_as_text(output_file="test-output.txt")
dungeon_map.output_as_img(output_file="test-output.png")
